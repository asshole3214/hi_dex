﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FeedbackForm.aspx.cs" Inherits="StudentFeedbackSystem.FeedbackForm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Feedback Form</title>
    <style type="text/css">
        .style1
        {
            height: 31px;
        }
        .style2
        {
            width: 57px;
        }
        .style3
        {
            height: 31px;
            width: 57px;
        }
        .style4
        {
            width: 549px;
        }
        .style5
        {
            height: 31px;
            width: 549px;
        }
        .style6
        {
            width: 438px;
        }
        .style7
        {
            height: 31px;
            width: 438px;
        }
        .style8
        {
            width: 611px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <br />
        <asp:Label ID="Label1" runat="server" Text="Adm No:"></asp:Label>
&nbsp;
        <asp:Label ID="lblAdmNo" runat="server"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label3" runat="server" Text="Name:"></asp:Label>
&nbsp;&nbsp;
        <asp:Label ID="lblName" runat="server"></asp:Label>
        <br />
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Size="Larger" 
            Text="Feedback on GEMs"></asp:Label>
        <br />
        <br />
        <asp:Label ID="Label5" runat="server" Text="1 = Strongly Disagree"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label6" runat="server" Text="2 = Disagree"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label7" runat="server" Text="3 = Neutral"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label8" runat="server" Text="4 = Agree"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label9" runat="server" Text="5 = Strongly Agree"></asp:Label>
        <br />
        <br />
        <table style="width: 80%;" border="1" cellspacing="0">
            <tr>
                <td align="center" class="style2" valign="middle">
                    &nbsp;</td>
                <td align="center" class="style4">
                    Question</td>
                <td class="style6">
                    &nbsp;</td>
                <td align="center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="center" class="style3" valign="middle">
                    1</td>
                <td class="style5">
                    I find that GEMs have broaden my knowledge in the area outside my course of 
                    study</td>
                <td class="style7">
                    <asp:RadioButtonList ID="radButtonList1" runat="server" 
                        RepeatDirection="Horizontal" Width="430px">
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td align="center" class="style1">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="radButtonList1" ErrorMessage="Compulsory" 
                        ForeColor="#CC3300"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td align="center" class="style2" valign="middle">
                    2</td>
                <td class="style4">
                    I find that GEMs have enhanced my interest in the related subject area</td>
                <td class="style6">
                    <asp:RadioButtonList ID="radButtonList2" runat="server" 
                        RepeatDirection="Horizontal" Width="430px">
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td align="center">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="radButtonList2" ErrorMessage="Compulsory" 
                        ForeColor="#CC3300"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td align="center" class="style2" valign="middle">
                    3</td>
                <td class="style4">
                    I am able to interact with students from other courses</td>
                <td class="style6">
                    <asp:RadioButtonList ID="radButtonList3" runat="server" 
                        RepeatDirection="Horizontal" Width="430px">
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td align="center">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="radButtonList3" ErrorMessage="Compulsory" 
                        ForeColor="#CC3300"></asp:RequiredFieldValidator>
                </td>
            </tr>
        </table>
        <table style="width: 80%;" border="1" cellspacing="0">
            <tr>
                <td align="center" class="style8">
                    Overall, I have benefit from the GEM programme</td>
                <td class="style6">
                    <asp:RadioButtonList ID="radButtonList4" runat="server" 
                        RepeatDirection="Horizontal" Width="430px">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td align="center">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                        ControlToValidate="radButtonList4" ErrorMessage="Compulsory" 
                        ForeColor="#CC3300"></asp:RequiredFieldValidator>
                </td>
            </tr>
        </table>
        <br />
        <asp:Label ID="Label10" runat="server" Text="Comments (Optional)" Width="111px"></asp:Label>
        <asp:TextBox ID="txtComments" runat="server" Height="72px" TextMode="MultiLine" 
            Width="600px"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnSubmit" runat="server" Text="Submit" Width="200px" 
            onclick="btnSubmit_Click" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <br />
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" style="margin-right: 0px" 
            Width="219px" DisplayMode="List" />
    
    </div>
    </form>
</body>
</html>
