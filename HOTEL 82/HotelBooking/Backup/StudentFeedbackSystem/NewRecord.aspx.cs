﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DataManagement;
namespace StudentFeedbackSystem
{
    public partial class NewRecord : System.Web.UI.Page
    {
        DataAccess da = new DataAccess();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //getAllSchools and add to school ddl
                DataTable dt = da.getAllSchools();
                ddlistSchool.DataSource = dt;
                ddlistSchool.DataTextField = "FullName";
                ddlistSchool.DataValueField = "School";
                ddlistSchool.DataBind();


                ddlistSchool.Items.Insert(0, "Select School");
                ddlistSchool.Items[0].Value = "";

                lblAdmNo.Text = Session["AdmNo"].ToString();
            }
            
        }

        protected void ddlistSchool_SelectedIndexChanged(object sender, EventArgs e)
        {
            string school = ddlistSchool.SelectedValue.ToString();

            DataTable dt = da.GetCourseBySchool(school);
            ddlistCourse.DataSource = dt;
            ddlistCourse.DataTextField = "FullName";
            ddlistCourse.DataValueField = "Course";
            ddlistCourse.DataBind();


            ddlistCourse.Items.Insert(0, "Select Course");
            ddlistCourse.Items[0].Value = "";
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                Student a = new Student(lblAdmNo.Text);
                a.Name = txtName.Text;
                a.School = ddlistSchool.SelectedValue.ToString();
                a.Course = ddlistCourse.SelectedValue.ToString();
                if (radGender.Text.ToString() == "")
                    a.Gender = " ";
                else
                    a.Gender = radGender.SelectedItem.ToString();

                if (txtGPA.Text != "")
                    a.GPA = Convert.ToDouble(txtGPA.Text);
                else
                    a.GPA = 0;
                a.Handphone = txtHandphone.Text;
                a.Religion = txtReligion.Text;
                a.Address = txtAddress.Text;

                da.insertStudent(a);

                da.insertFeedback(a.AdmNo);


                Session["Name"] = txtName.Text;
                Response.Redirect("FeedbackForm.aspx");
            }
        }
    }
}