﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="StudentFeedbackSystem.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Feedback on GEMs</title>
</head>
<body style="FONT-SIZE: 12pt">
    <form id="form1" runat="server">
    <div>
    
        <asp:Panel ID="Panel1" runat="server" BackColor="#0000A0" Height="488px" 
            HorizontalAlign="Center" Width="804px">
            <br />
            <br />
            <asp:Label ID="Label1" runat="server" Text="Singapore Polytechnic" 
                Font-Size="X-Large" ForeColor="White"></asp:Label>
            <br />
            <br />
            <asp:Label ID="Label2" runat="server" Font-Size="Larger" ForeColor="White" 
                Text="Feedback on GEMs"></asp:Label>
            <br />
            <br />
            <asp:Label ID="Label3" runat="server" Font-Size="Medium" ForeColor="White" 
                Text="Admission No. (eg 0812345)" Width="106px"></asp:Label>
&nbsp;
            <asp:TextBox ID="txtAdmNo" runat="server" Height="40px" 
                CausesValidation="True"></asp:TextBox>
            &nbsp;&nbsp;
            <asp:Button ID="btnGo" runat="server" onclick="btnGo_Click" Text="Go..." />
            <br />
            <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                ControlToValidate="txtAdmNo" ErrorMessage="Please enter Admission No." 
                ForeColor="Red"></asp:RequiredFieldValidator>
            <br />
            <asp:Label ID="lblDone" runat="server" Font-Bold="True" ForeColor="#CCCC00" 
                Text="You Have Done Your Feedback. Thank You!" Visible="False"></asp:Label>
            <br />
            <asp:Label ID="lblNotExist" runat="server" Font-Bold="True" ForeColor="#FF9900" 
                Text="Your Admission Number is not in the Database!" Visible="False"></asp:Label>
            <br />
            <br />
            <asp:Button ID="btnRegister" runat="server" Text="Register Now..." 
                Visible="False" onclick="btnRegister_Click" />
            <br />
            <br />
        </asp:Panel>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
    
    </div>
    </form>
</body>
</html>
