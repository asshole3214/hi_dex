﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DataManagement;

namespace StudentFeedbackSystem
{
    public partial class Login : System.Web.UI.Page
    {
        DataAccess da = new DataAccess();
        protected void Page_Load(object sender, EventArgs e)
        {
            txtAdmNo.Focus();
        }

        
        protected void btnRegister_Click(object sender, EventArgs e)
        {
            Session["AdmNo"] = txtAdmNo.Text;
            Response.Redirect("NewRecord.aspx");
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                DataTable dt = da.GetStudentsByAdmNo(txtAdmNo.Text);
                if (dt.Rows.Count == 0)
                {
                    lblNotExist.Visible = true;
                    btnRegister.Visible = true;
                }
                else
                {
                    DataTable dtFeedback = da.GetFeedbackByAdmNo(txtAdmNo.Text);
                    if (dtFeedback.Rows[0][6].ToString() == "Y")
                        lblDone.Visible = true;
                    else
                        Response.Redirect("FeedbackForm.aspx");
                }


            }

        }
    }
}