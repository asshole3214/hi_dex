﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DataManagement;

namespace StudentFeedbackSystem
{
    public partial class SerachCustermerForm : System.Web.UI.Page
    {
        DataAccess dataAccess = new DataAccess();
        protected void Page_Load(object sender, EventArgs e)
        {
            lblError.Visible = false;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if(Page.IsValid)
            {
                DataTable CustermerInfo = dataAccess.GetCustermerByNRIC(txtCustermerLookUp.Text);
                if (CustermerInfo.Rows.Count > 0)
                {
                    Session["CustermerUserName"] = CustermerInfo.Rows[0].ItemArray[1];
                    if (CustermerInfo.Rows[0].ItemArray[9] != "")
                    {   
                        Response.Redirect("InfoPage.aspx");
                    }
                    else
                    {
                        Response.Redirect("NewRoomBooking.aspx");
                    }
                }
                else
                {
                    lblError.Visible = true;
                }
            }
        }
    }
}