﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DataManagement;

namespace StudentFeedbackSystem
{
    public partial class Login : System.Web.UI.Page
    {
        DataAccess da = new DataAccess();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                txtUserName.Focus();
                lblFail.Visible = false;
            }
        }

        
        protected void btnRegister_Click(object sender, EventArgs e) // btnCustermer 
        {
            Response.Redirect("NewCustermerSignUp.aspx");
        }

        protected void btnGo_Click(object sender, EventArgs e) //btnGo
        {
            Page.Validate();
            if (Page.IsValid)
            {
                DataTable dtCustermer = da.GetCustermerByAccount(txtUserName.Text,txtPassword.Text);
                if (dtCustermer.Rows.Count == 0) // if no records exist in the database
                {
                    lblFail.Visible = true;
                }
                else // when record exist 
                {
                    if (dtCustermer.Rows[0][3].ToString() == "Staff") // checking if user is staff or custermer
                    {
                        Session["StaffUserName"] = txtUserName.Text;
                        //lblDone.Visible = true; // redirect staff to staff page here
                        Response.Redirect("StaffPage.aspx");
                    }
                    else
                    {
                        Session["CustermerUsername"] = txtUserName.Text;
                        if (dtCustermer.Rows[0][9].ToString() == "") // checking if user is checked into the hotel (checking on RoomNo)
                        {
                            Response.Redirect("NewRoomBooking.aspx"); // redirect to booking page here 
                        }
                        else
                        {
                            Response.Redirect("InfoPage.aspx"); // redirect to infopage here
                        }
                    }
                        
                }
            }
        }
    }
}