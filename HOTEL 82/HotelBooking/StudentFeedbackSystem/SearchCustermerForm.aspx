﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchCustermerForm.aspx.cs" Inherits="StudentFeedbackSystem.SerachCustermerForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Search For Customer</title>
    <link rel="Stylesheet" type="text/css" href="css.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div style="background-color:#f1f1f1">
            <center>
                <div>
                    <br />
                        <table cellpadding="5" cellspacing="5" style="width: 80%; margin-top: 0px; background-color:white">
                             <tr class="redbg"><!--welcome label-->
                                 <td colspan="3">
                                     <center>
                                         <asp:Label ID="lblSearchCustomers" runat="server" Text="Customer Look Up" Font-Size="XX-Large" Font-Bold="True" ForeColor="White"></asp:Label>
                                     </center>
                                 </td>
                             </tr>
                            
                             <tr> <!-- CustermerLookUp row-->
                                <td>
                                    <asp:Label ID="lblCustermerLookUp" runat="server" Text ="Customer NRIC :" ></asp:Label>
                                </td>
                                <td>
                                     <asp:TextBox ID ="txtCustermerLookUp" runat ="server" Width ="600px"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                        ControlToValidate="txtCustermerLookUp" ErrorMessage="Compulsory" 
                                        ForeColor="#CC3300"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr><!--Button row-->
                                <td></td>
                                <td>
                                    <center>
                                         <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" CssClass="button" />
                                    </center>
                                </td>
                            </tr>
                            <tr><!--Error row-->
                                <td></td>
                                <td>
                                    <center>
                                         <asp:Label ID="lblError" runat="server" Text="No Record Exist please check " ForeColor="#CC3300"  ></asp:Label>
                                    </center>
                                </td>
                            </tr>
                        </table>
                    <br />
                </div>
            </center>
        </div>
    </form>
</body>
</html>
