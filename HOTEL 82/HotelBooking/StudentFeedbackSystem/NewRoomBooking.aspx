﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewRoomBooking.aspx.cs" Inherits="StudentFeedbackSystem.NewRecord" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>RoomBooking page</title>
    <link rel="Stylesheet" type="text/css" href="css.css" />
    <style type="text/css">
        .auto-style1 {
            width: 147px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="background-color: #f1f1f1">
        <table style="width: 80%;" align ="center" bgcolor="white">
            <tr align="center" class="redbg">
                <td colspan="3">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="XX-Large" Text="New Room Booking" ForeColor="White"></asp:Label>
                </td>
            </tr>
            <tr class="redbg">
                <td colspan="3">
                    <asp:Label ID="Label2" runat="server" Text="Welcome " ForeColor="White"></asp:Label>
                    &nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lblName" runat="server" ForeColor="White"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class ="auto-style1">
                    Number of People 
                </td>
                <td>
                    <asp:TextBox ID ="txtNoOfPeople" runat ="server" Width ="160px"></asp:TextBox>
                </td>
                 <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="txtNoOfPeople" ErrorMessage="Compulsory" 
                        ForeColor="#CC3300"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    Room Types</td>
                <td class="style2">
                    <asp:DropDownList ID="ddlistRoomTypes" runat="server" AutoPostBack="True" 
                        onselectedindexchanged="ddlistSchool_SelectedIndexChanged" Width="200px">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="ddlistRoomTypes" ErrorMessage="Compulsory" 
                        ForeColor="#CC3300"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    Rooms Avaliable</td>
                <td class="style2">
                    <asp:DropDownList ID="ddlistRoomSelected" runat="server" Width="200px">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="ddlistRoomSelected" ErrorMessage="Compulsory" 
                        ForeColor="#CC3300"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    Check-in Date</td>
                <td class="auto-style3">
                    <asp:Calendar ID="CalendarCheckIn" runat="server" OnSelectionChanged="CalendarCheckIn_SelectionChanged">
                        <SelectedDayStyle BackColor="#6666FF" />
                    </asp:Calendar>
                </td>
                <td>
                    <asp:customvalidator id="CalendarCheckInValidator"  OnServerValidate="CalendarCheckIn_Validate" runat="server" ErrorMessage ="Compulsory" ForeColor ="#CC3300"></asp:customvalidator>
                        
                    <br />
                        
                    <br />
                    <asp:CustomValidator ID="CompareDateValidator" runat="server" ErrorMessage ="Invalid Date Range" ForeColor ="#CC3300" OnServerValidate="CompareDateValidator_ServerValidate"></asp:CustomValidator>
                        
                </td>
            </tr>
               <br />
            <tr>
                <td class="auto-style1">
                    Check-out Date</td>
                <td class="style2">
                   <asp:Calendar ID="CalendarCheckOut" runat="server" OnSelectionChanged="CalendarCheckOut_SelectionChanged">
                       <SelectedDayStyle BackColor="#6666FF" />
                    </asp:Calendar>
                </td>
                <td>
                    <asp:customvalidator id="CalendarCheckOutValidator"  OnServerValidate="CalendarCheckOut_Validate" runat="server" ErrorMessage ="Compulsory" ForeColor ="#CC3300"></asp:customvalidator>
                </td>
            </tr>
            <tr>
                <td class ="auto-style1">
                    Comments (optional)
                </td>
                <td>
                    <asp:TextBox ID ="txtComments" runat ="server" Width ="533px"></asp:TextBox>
                </td>
                 <td>

                </td>
            </tr>
        </table>
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnRegister" runat="server" Text="Register" 
            onclick="btnRegister_Click" />
        <br />
        <br />
&nbsp;
    
    </div>
    </form>
</body>
</html>
