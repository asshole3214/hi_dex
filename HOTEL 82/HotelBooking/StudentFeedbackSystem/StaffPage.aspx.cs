﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DataManagement;

namespace StudentFeedbackSystem
{
    public partial class StaffPage : System.Web.UI.Page
    {
        DataAccess dataAccess = new DataAccess();
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable staff = dataAccess.GetCustermerByUserName((string)Session["StaffUserName"]);
            lblStaffName.Text = staff.Rows[0].ItemArray[4].ToString();
        }

        protected void btnCustermerCheckIn_Out_Clicked(object sender, EventArgs e)
        {
            Response.Redirect("SearchCustermerForm.aspx");
        }

        protected void btnNewStaff_Clicked(object sender, EventArgs e)
        {
            Session["StaffCheck"] = true;
            Response.Redirect("NewCustermerSignUp.aspx");
        }
    }
}