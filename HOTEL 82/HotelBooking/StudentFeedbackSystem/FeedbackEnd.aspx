﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FeedbackEnd.aspx.cs" Inherits="StudentFeedbackSystem.FeedbackEnd" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>End of Feedback</title>
    <link rel="Stylesheet" type="text/css" href="css.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <center>
            <asp:Panel ID="Panel1" runat="server" BackColor="#FF5050" Height="500px" HorizontalAlign="Center" Width="800px">
               <div>
                   <br />
                   <br />
                   <br />
                   <br />
                   <br />
                   <br />
                   <br />
                   <asp:Label ID="lblThankYou" runat="server" Text="Thank you for your feedback." 
                    Font-Size="X-Large" ForeColor="White"></asp:Label>
                <br />
                   <br />
                   <br />
                <br />
                   <asp:Button ID="btnReturn" runat="server" Text="Return" CssClass="button" OnClick="btnReturn_Click" BackColor="#CCCCCC" ForeColor="Black" /> 
               </div>
            </asp:Panel>
        </center>
        <br />
    
    </div>
    </form>
</body>
</html>
