﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DataManagement;

namespace StudentFeedbackSystem
{
    public partial class FeedbackForm : System.Web.UI.Page
    {
        DataAccess da = new DataAccess();
        DataTable CustermerInfo = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CustermerInfo = da.GetCustermerByUserName((string)Session["CustermerUserName"]);
                lblnric.Text = CustermerInfo.Rows[0].ItemArray[0].ToString(); //Custermer NRIC  
                lblName.Text = CustermerInfo.Rows[0].ItemArray[4].ToString(); //Custermer Name
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            CustermerInfo = da.GetCustermerByUserName((string)Session["CustermerUserName"]);
            RoomBooking room = new RoomBooking(CustermerInfo.Rows[0].ItemArray[1].ToString());
            Page.Validate();
            if (Page.IsValid)
            {
                int q1 = Convert.ToInt32(radButtonList1.SelectedValue);
                int q2 = Convert.ToInt32(radButtonList2.SelectedValue);
                int q3 = Convert.ToInt32(radButtonList3.SelectedValue);
                double aver = (q1 + q2 + q3) / 3.0;
                Feedback feedback = new Feedback(lblnric.Text, q1, q2, q3, radButtonList4.SelectedValue, txtComments.Text, aver);
                da.UploadFeedback(feedback);
                da.updateHotelRoom(CustermerInfo.Rows[0].ItemArray[9].ToString(), "".ToString(), 0);
                da.ClearingBookingInfo(room); // clearing the record of custermer staying there

                Response.Redirect("FeedbackEnd.aspx");
            }
        }
    }
}