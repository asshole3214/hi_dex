﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FeedbackForm.aspx.cs" Inherits="StudentFeedbackSystem.FeedbackForm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Feedback Form</title>
    <link rel="Stylesheet" type="text/css" href="css.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="background-color:#f1f1f1">
        <table style="width: 80%; background-color:white;" border="1" cellspacing="0" align="center" >
            <tr bgcolor = "#FF5050">
                <td colspan="2" >
                    <asp:Label ID="Label1" runat="server" Text="NRIC :" ForeColor="White"></asp:Label>
                    &nbsp;
                    <asp:Label ID="lblnric" runat="server" ForeColor="White"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:Label ID="Label3" runat="server" Text="Name:" ForeColor="White"></asp:Label>
                    &nbsp;
                    <asp:Label ID="lblName" runat="server" ForeColor="White"></asp:Label>
                </td>
            </tr>
            <tr bgcolor="#FF5050">
                <td colspan="4"><asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Size="Larger" 
            Text="Feedback on Customer Service" ForeColor="White"></asp:Label></td>
            </tr>
            <tr bgcolor="FF5050">
                <td colspan="4"><asp:Label ID="Label5" runat="server" Text="1 = Very Disatisfied" ForeColor="White"></asp:Label>
&nbsp;&nbsp;
        <asp:Label ID="Label6" runat="server" Text="2 = Disatisfied" ForeColor="White"></asp:Label>
&nbsp;&nbsp;
        <asp:Label ID="Label7" runat="server" Text="3 = Neutral" ForeColor="White"></asp:Label>
                    &nbsp;&nbsp;&nbsp;<asp:Label ID="Label8" runat="server" Text="4 = Satisfied" ForeColor="White"></asp:Label>
&nbsp;&nbsp;
        <asp:Label ID="Label9" runat="server" Text="5 = Very Satisfied" ForeColor="White"></asp:Label></td>
            </tr>
            <tr>
                <td align="center" class="style3" valign="middle">
                    1</td>
                <td class="style5">
                    Staff friendliness</td>
                <td class="style7">
                    <asp:RadioButtonList ID="radButtonList1" runat="server" 
                        RepeatDirection="Horizontal" Width="430px">
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td align="center" class="style1">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="radButtonList1" ErrorMessage="Compulsory" 
                        ForeColor="#CC3300"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td align="center" class="style2" valign="middle">
                    2</td>
                <td class="style4">
                    Speed of service</td>
                <td class="style6">
                    <asp:RadioButtonList ID="radButtonList2" runat="server" 
                        RepeatDirection="Horizontal" Width="430px">
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td align="center">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="radButtonList2" ErrorMessage="Compulsory" 
                        ForeColor="#CC3300"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td align="center" class="style2" valign="middle">
                    3</td>
                <td class="style4">
                    Availabilty of staffs</td>
                <td class="style6">
                    <asp:RadioButtonList ID="radButtonList3" runat="server" 
                        RepeatDirection="Horizontal" Width="430px">
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td align="center">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="radButtonList3" ErrorMessage="Compulsory" 
                        ForeColor="#CC3300"></asp:RequiredFieldValidator>
                </td>
            </tr>

        </table>
        <table style="width: 80%; background-color:white;" border="1" cellspacing="0" align="center">
            <tr>
                <td>
                    Would you reccomend this hotel to your friends and family?<br />
                </td>
                <td>
                    <asp:RadioButtonList ID="radButtonList4" runat="server" 
                        RepeatDirection="Horizontal" Width="430px">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td align="center">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                        ControlToValidate="radButtonList4" ErrorMessage="Compulsory" 
                        ForeColor="#CC3300"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Comments (Optional)</td>
                <td colspan="2"><asp:TextBox ID="txtComments" runat="server" Height="72px" TextMode="MultiLine" 
            Width="600px"></asp:TextBox></td>
            </tr>
            <tr>
                <td><asp:Button ID="btnSubmit" CssClass="button" runat="server" Text="Submit" Width="200px" 
            onclick="btnSubmit_Click" /></td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
