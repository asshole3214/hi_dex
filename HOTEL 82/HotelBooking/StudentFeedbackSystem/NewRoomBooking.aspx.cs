﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DataManagement;
using System.Collections.Generic;
namespace StudentFeedbackSystem
{
    public partial class NewRecord : System.Web.UI.Page
    {
        DataAccess dataAccess = new DataAccess();
        DataTable CustermerInfo = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CustermerInfo =dataAccess.GetCustermerByUserName((string)Session["CustermerUserName"]);
                lblName.Text = CustermerInfo.Rows[0].ItemArray[4].ToString(); // getting name of custermer
                //lblUsername.Text = 
                //getAllSchools and add to school ddl
                DataTable dtRoomTypes = dataAccess.GetRoomTypes();
                ddlistRoomTypes.DataSource = dtRoomTypes;
                ddlistRoomTypes.DataTextField = "FullName";
                ddlistRoomTypes.DataValueField = "RoomType";
                ddlistRoomTypes.DataBind();
                ddlistRoomTypes.Items.Insert(0, "Select Room Type");
                ddlistRoomTypes.Items[0].Value = "";
            }

        }

      

        protected void ddlistSchool_SelectedIndexChanged(object sender, EventArgs e)
        {
            string RoomType = ddlistRoomTypes.SelectedValue.ToString();

            DataTable dtRoomAvaliable = dataAccess.GetAvaliableRoom(RoomType);
            ddlistRoomSelected.DataSource = dtRoomAvaliable;
            ddlistRoomSelected.DataTextField = "RoomName";
            ddlistRoomSelected.DataValueField = "RoomNo";
            ddlistRoomSelected.DataBind();
            ddlistRoomSelected.Items.Insert(0, "Select Room");
            ddlistRoomSelected.Items[0].Value = "";

        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                // creating a custermer class
                RoomBooking roomBook = new RoomBooking(ddlistRoomSelected.SelectedItem.Value.ToString(), CalendarCheckIn.SelectedDate.ToString(), CalendarCheckOut.SelectedDate.ToString(), txtComments.Text, (string)Session["CustermerUsername"], txtNoOfPeople.Text);
                dataAccess.UploadBookingInfo(roomBook);
                dataAccess.updateHotelRoom(roomBook.roomSelection, roomBook.userName, -1);
                Response.Redirect("SubmittedEndPage.aspx");
          
            }  
        }


        protected void CalendarCheckIn_Validate (object sender, ServerValidateEventArgs args)
        {
            if(CalendarCheckIn.SelectedDate == new DateTime())
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }
        protected void CalendarCheckOut_Validate(object sender, ServerValidateEventArgs args)
        {
            if (CalendarCheckOut.SelectedDate == new DateTime())
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }
     
        protected void CalendarCheckIn_SelectionChanged(object sender, EventArgs e)
        {

        }
        protected void CalendarCheckOut_SelectionChanged(object sender, EventArgs e)
        {

        }

        protected void CompareDateValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            DateTime d1 = CalendarCheckIn.SelectedDate;
            DateTime d2 = CalendarCheckOut.SelectedDate;

            if (d1 > d2)
            {
                args.IsValid = false;
            } else
            {
                args.IsValid = true;
            }
        }
    }
}