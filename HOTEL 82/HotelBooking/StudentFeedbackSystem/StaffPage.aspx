﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StaffPage.aspx.cs" Inherits="StudentFeedbackSystem.StaffPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Staff</title>
    <link rel="Stylesheet" type="text/css" href="css.css" />    
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Panel ID="Panel1" runat="server" BackColor="#FF5050" Height="500px" HorizontalAlign="Center" Width="800px">
            <center>
                <!--let all text color in this page be #009900 please use this for consistency-->
                <center>
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <asp:Label ID="lblWelcome" runat="server" ForeColor="White" Text ="Welcome "></asp:Label>
                     <asp:Label ID="lblStaffName" runat="server" ForeColor ="White"></asp:Label>
                     <br /> 
                     <br />
                     <asp:Button ID="btnCustermerCheckIn_Out" runat="server" ForeColor="Black" OnClick ="btnCustermerCheckIn_Out_Clicked" Text="Check Customer Status" Width ="300px" BackColor="Silver" CssClass="button"/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <asp:Button ID="btnNewStaff" runat="server" ForeColor="Black" OnClick ="btnNewStaff_Clicked" Text="Add New Staff" Width="300px" BackColor="Silver" CssClass="button"/>
                </center>
               </asp:Panel>
            </center>
        </div>
    </form>
</body>
</html>
