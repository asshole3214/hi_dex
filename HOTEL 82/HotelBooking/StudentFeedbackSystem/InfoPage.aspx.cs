﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DataManagement;

namespace StudentFeedbackSystem
{
    public partial class InfoPage : System.Web.UI.Page
    {
        DataAccess da = new DataAccess();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt= da.GetCustermerByUserName((string)Session["CustermerUserName"]);
            lblUserName.Text = dt.Rows[0].ItemArray[4].ToString();
        }

        protected void btnCheckOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("FeedbackForm.aspx");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Session["CustomerUserName"] = "";
            Response.Redirect("Login.aspx");
        }
    }
}