﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewCustermerSignUp.aspx.cs" Inherits="StudentFeedbackSystem.NewCustermerSignUp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>Customer Sign Up </title>
        <link rel="Stylesheet" type="text/css" href="css.css" />
        <style type="text/css">
            .auto-style2 {
                width: 218px;
            }
        </style>
    </head>
    <body>
        <form id="form1" runat="server">
            <div style="background-color:#f1f1f1">
                    <!-- let all text color in this page be #FFFFFF please use this for consistency-->
                    <table  style="width: 80%; margin-top: 0px; background-color:#fff;" align="center">
                        <tr class ="redbg" align="center">
                            <td colspan="3">
                                <asp:Label ID="lblWelcomeTag" runat="server" Text ="Sign Up" Font-Bold="True" ForeColor="White"></asp:Label>
                            </td>
                        </tr>
                        <tr> <!-- Nric row-->
                            <td class="auto-style2">
                                <asp:Label ID="lblNRIC" runat="server" Text ="NRIC Number :" ></asp:Label>
                            </td>
                            <td>
                                 <asp:TextBox ID ="txtNRIC" runat ="server" Width ="600px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                    ControlToValidate="txtNRIC" ErrorMessage="Compulsory" ForeColor="#CC3300"
                                    ></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                         <tr> <!-- Username Row -->
                            <td class="auto-style2">
                                <asp:Label ID="lblUserName" runat="server" Text ="Username :" ></asp:Label>
                            </td>
                            <td>
                                 <asp:TextBox ID ="txtUserName" runat ="server" Width ="600px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                    ControlToValidate="txtUserName" ErrorMessage="Compulsory" ForeColor="#CC3300"
                                    ></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                         <tr> <!-- Password Row-->
                            <td class="auto-style2">
                                <asp:Label ID="lblPassword" runat="server" Text ="Password :"></asp:Label>
                            </td>
                            <td>
                                 <asp:TextBox ID ="txtPassword" runat ="server" Width ="600px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                    ControlToValidate="txtPassword" ErrorMessage="Compulsory" ForeColor="#CC3300"
                                    ></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                         <tr> <!-- Name Row-->
                            <td class="auto-style2">
                                <asp:Label ID="lblName" runat="server" Text ="Name :" ></asp:Label>
                            </td>
                            <td>
                                 <asp:TextBox ID ="txtName" runat ="server" Width ="600px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                    ControlToValidate="txtName" ErrorMessage="Compulsory" ForeColor="#CC3300"
                                    ></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                         <tr> <!-- Age Row-->
                            <td class="auto-style2">
                                <asp:Label ID="lblAge" runat="server" Text ="Age:"></asp:Label>
                            </td>
                            <td>
                                 <asp:TextBox ID ="txtAge" runat ="server" Width ="600px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                                    ControlToValidate="txtAge" ErrorMessage="Compulsory" ForeColor="#CC3300"
                                    ></asp:RequiredFieldValidator>
                                <br />
                                <asp:CompareValidator ID="CompareValidator1" runat ="server" ControlToValidate="txtAge" Type="Integer" Operator="DataTypeCheck" ErrorMessage ="Please Put A valid Number" ForeColor ="#CC3300"></asp:CompareValidator>
                            </td>
                        </tr>
                         
                         <tr> <!-- Nationality Row-->
                            <td class="auto-style2">
                                <asp:Label ID="lblNationality" runat="server" Text ="Nationality :" ></asp:Label>
                            </td>
                            <td>
                                 <asp:TextBox ID ="txtNationality" runat ="server" Width ="600px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                                    ControlToValidate="txtNationality" ErrorMessage="Compulsory" ForeColor="#CC3300"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                         <tr> <!-- Gender Row-->
                            <td class="auto-style2">
                                <asp:Label ID="lblGender" runat="server" Text ="Gender :"></asp:Label>
                            </td>
                            <td>
                                 <asp:DropDownList ID="ddlistGender" runat="server" AutoPostBack="True" 
                                    onselectedindexchanged="ddlistGender_SelectedIndexChanged" Width="600px">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                                    ControlToValidate="ddlistGender" ErrorMessage="Compulsory" ForeColor="#CC3300"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                         <tr> <!-- HP Row-->
                            <td class="auto-style2">
                                <asp:Label ID="lblHP" runat="server" Text ="Handphone Number :"></asp:Label>
                            </td>
                            <td>
                                 <asp:TextBox ID ="txtHP" runat ="server" Width ="600px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
                                    ControlToValidate="txtHp" ErrorMessage="Compulsory" ForeColor="#CC3300"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr> <!-- Address Row-->
                            <td class="auto-style2">
                                <asp:Label ID="lblAddress" runat="server" Text ="Address :"></asp:Label>
                            </td>
                            <td class="auto-style3">
                                 <asp:TextBox ID ="txtAddress" runat ="server" Width ="600px"></asp:TextBox>
                            </td>
                            <td class="auto-style3">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" 
                                    ControlToValidate="txtAddress" ErrorMessage="Compulsory" 
                                    ForeColor="#CC3300"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr align="center">
                            <td colspan="3">
                                <asp:Button ID="btnSubmit" CssClass="button" runat="server" Text="Submit" OnClick="Submitbtn_Click"/>
                            </td>
                        </tr>
                    </table>
            </div>
        </form>
        </body>
</html>
