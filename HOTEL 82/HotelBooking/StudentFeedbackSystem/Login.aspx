﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="StudentFeedbackSystem.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title>Hotel 82</title>
   <link rel="Stylesheet" type="text/css" href="css.css" />
</head>
<body style="background-image:url(images/hotel.jpg);">
    <form id="form1" runat="server">
    <div>
        <table align ="center" style="background-color:white">
            <tr align="center">
                <td colspan="3" bgcolor="#FF5050"><asp:Label ID="Label1" runat="server" Text="Hotel 82" 
                    Font-Size="X-Large" ForeColor="White"></asp:Label>
                <br />
                <asp:Label ID="Label2" runat="server" Font-Size="Larger" ForeColor="White" 
                    Text="Customer Service"></asp:Label></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblUserName" runat="server" Font-Size="Large" ForeColor="Black" 
                Text="Username: " Width="140px" style="margin-right: 0px; margin-left: 0px;"></asp:Label></td>
                <td><asp:TextBox ID="txtUserName" runat="server" Height="20px" 
                        CausesValidation="True"></asp:TextBox></td>
                <td><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="txtUserName" ErrorMessage="Please enter Username" 
                        ForeColor="#CC0000" Font-Size="Small"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblPassword" runat="server" Font-Size="Large" ForeColor="Black" 
                Text="Password: " Width="140px" style="margin-right: 0px; margin-left: 0px;"></asp:Label>
                    </td>
                <td><asp:TextBox ID="txtPassword" runat="server" Height="20px" 
                    CausesValidation="True" TextMode="Password"></asp:TextBox></td>
                <td><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="txtPassword" ErrorMessage="Please enter Password" 
                        ForeColor="#CC0000" Font-Size="Small"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td class="auto-style1"></td>
                <td class="auto-style1"><asp:Button ID="btnGo" runat="server" onclick="btnGo_Click" Text="Login" CssClass="button" /></td>
                <td class="auto-style1"><asp:Label ID="lblFail" runat="server" Text="Incorrect Username &amp; Password" ForeColor="#CC0000" Font-Size="Small"></asp:Label></td>
            </tr>  
            <tr>
                <td><asp:Label ID="lblNotExist" runat="server" Font-Bold="True" ForeColor="Black"
                    Text="New user here? "></asp:Label></td>
                <td><asp:Button ID="btnCustomer" CssClass="button" runat="server" Text="Register Now" onclick="btnRegister_Click" CausesValidation="False" Width="180px" /></td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>
</body>
</html>
