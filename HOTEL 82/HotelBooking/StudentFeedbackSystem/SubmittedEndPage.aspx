﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SubmittedEndPage.aspx.cs" Inherits="StudentFeedbackSystem.SubmittedEndPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Request Submitted</title>
    <link rel="Stylesheet" type="text/css" href="css.css" />
    <style type="text/css">
        .auto-style1 {
            height: 506px;
        }
    </style>
</head>
    <body style="vertical-align: middle; height: 100%;">
        <form id="form1" runat="server">
            <center class="auto-style1">
                  <!-- let all text color in this page be #009900 please use this for consistency-->
                  <div>
                      <br />
                      <br />
                      <br />
                      <br />
                      <br />
                      <br />
                      <asp:Label ID="lblSubmitted" runat="server" Text="Your request have been submitted"></asp:Label>
                  </div>
                  <br />
                  <br />
                  <asp:Button ID="btnReturn" runat ="server" Text="Return to Login Page" OnClick ="btnReturn_Clicked" CssClass="button" />
            </center>
        </form>
    </body>
</html>
