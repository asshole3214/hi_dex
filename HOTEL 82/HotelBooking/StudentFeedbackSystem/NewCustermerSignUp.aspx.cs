﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataManagement;

namespace StudentFeedbackSystem
{
    public partial class NewCustermerSignUp : System.Web.UI.Page
    {
        List<string> Genders = new List<string>();
        DataAccess dataAccess = new DataAccess();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                Genders.Add("M");
                Genders.Add("F");

                ddlistGender.DataSource = Genders;
                ddlistGender.DataBind();
                ddlistGender.Items.Insert(0 , "Select Gender");
                ddlistGender.Items[0].Value="";

                Session["StaffCheck"] = false;
            }
        }

        protected void ddlistGender_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void Submitbtn_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if(Page.IsValid)
            {
                //string _NRIC, string _Name, string _Age, string _Nationality, string _Gender, string _HP, string _Address
                CustomersInfo customer = new CustomersInfo(txtNRIC.Text,txtUserName.Text,txtPassword.Text,txtName.Text,txtAge.Text,txtNationality.Text,ddlistGender.SelectedItem.Text,txtHP.Text,txtAddress.Text);
                if((bool)Session["StaffCheck"]==true)
                {
                    customer.accessType = "Staff";
                } else
                {
                    customer.accessType = "Customer";
                }
                dataAccess.InsertNewUser(customer);
                Response.Redirect("SubmittedEndPage.aspx");
            }
        }
    }
}