﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataManagement
{
    public class Feedback
    {
        Random ranNo = new Random();
        private string _NRIC;
        private int _Q1;
        private int _Q2;
        private int _Q3;
        private string _BenefitHotel;
        private string _Comments;
        private string _Done;
        private double _Ave;
        private string _RandNo;

        public Feedback(string NRIC, int Q1, int Q2, int Q3, string BenefitHotel, string Comment ,double Ave)
        {
            this._NRIC = NRIC;
            this._Q1 = Q1;
            this._Q2 = Q2;
            this._Q3 = Q3;
            this._BenefitHotel = BenefitHotel;
            this._Comments = Comment;
            this._Done = "Y";
            this._Ave = Ave;
            this._RandNo = ranNo.Next(0, 2147483641).ToString();

        }


        public string NRIC
        {
            get
            {
                return _NRIC;
            }

            set
            {
                _NRIC = value;
            }
        }

        public int Q1
        {
            get
            {
                return _Q1;
            }

            set
            {
                _Q1 = value;
            }
        }


        public int Q2
        {
            get
            {
                return _Q2;
            }

            set
            {
                _Q2 = value;
            }
        }

        public int Q3
        {
            get
            {
                return _Q3;
            }

            set
            {
                _Q3 = value;
            }
        }

        public string BenefitHotel 
        {
            get
            {
                return _BenefitHotel;
            }

            set
            {
                _BenefitHotel = value;
            }
        }

        public string Comments
        {
            get
            {
                return _Comments;
            }

            set
            {
                _Comments = value;
            }
        }

        public string Done
        {
            get
            {
                return _Done;
            }

            set
            {
                _Done = value;
            }
        }

        public double Ave
        {
            get
            {
                return _Ave;
            }

            set
            {
                _Ave = value;
            }
        }
        public string RanNo
        {
            get
            {
                return this._RandNo;
            }
            set
            {
                this._RandNo = value;
            }
        }
       
    }
}
