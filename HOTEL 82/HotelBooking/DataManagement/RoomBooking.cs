﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataManagement
{
    public class RoomBooking
    {
        private string UserName;
        private string RoomSelection;
        private string CheckInDate;
        private string CheckOutDate;
        private string Comments;
        private string NoOfPeople;

        public RoomBooking(string _RoomSelection , string _CheckInDate , string _CheckOutDate , string _Comments, string _UserName,string _NoOfPeople)
        {
            this.RoomSelection = _RoomSelection;
            this.checkInDate = _CheckInDate;
            this.CheckOutDate = _CheckOutDate;
            this.Comments = _Comments;
            this.UserName = _UserName;
            this.NoOfPeople = _NoOfPeople;
        }
        public RoomBooking(string _UserName)
        {
            this.RoomSelection = "";
            this.checkInDate = "";
            this.CheckOutDate = "";
            this.Comments = "";
            this.UserName = _UserName;
            this.NoOfPeople = "0";
        }

        public RoomBooking()
        {
            this.RoomSelection = "";
            this.checkInDate = "";
            this.CheckOutDate = "";
            this.Comments = "";
            this.UserName = "";
            this.NoOfPeople = "";
        }

        public string roomSelection
        {
            get { return this.RoomSelection; }
            set { this.RoomSelection = value; }
        }
        public string checkInDate
        {
            get { return this.CheckInDate; }
            set { this.CheckInDate = value; }
        }
        public string checkOutDate
        {
            get { return this.CheckOutDate; }
            set { this.CheckOutDate = value; }
        }
        public string comments
        {
            get { return this.Comments; }
            set { this.Comments = value; }
        }
        public string userName
        {
            get { return this.UserName; }
            set { this.UserName = value; }
        }
        public string noOfPeople
        {
            get { return this.NoOfPeople; }
            set { this.NoOfPeople = value; }
        }

    }
}
