﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;

namespace DataManagement
{
    public class DataAccess
    {
        public string connection = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=";
        /*For Access 2007 and 2010, please use
            connection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=D:\\Students.accdb;Persist Security Info=False;";
	    */
        public DataAccess()
        {
            string dir = System.AppDomain.CurrentDomain.BaseDirectory;
            string pdir = System.IO.Directory.GetParent(dir).ToString();
            string ppdir = System.IO.Directory.GetParent(pdir).ToString();
            string path = ppdir + "\\Database\\";
            this.connection = this.connection + path + "HotelDatabase.mdb";
        }


        public DataTable GetCustermerByAccount(string Username , string Password) // used for password validation
        {
            DataTable dt = new DataTable();

            //create connection 
            OleDbConnection dbConnection = new OleDbConnection();
            dbConnection.ConnectionString = this.connection;

            //build the command 
            OleDbCommand dbCommand = new OleDbCommand();
            dbCommand.CommandText = Sql.GetCustermerByAccount(Username,Password);
            dbCommand.Connection = dbConnection;

            //create adaptor
            OleDbDataAdapter dbAdap = new OleDbDataAdapter();
            dbAdap.SelectCommand = dbCommand;

            //retrieve data and put in database
            DataSet ds = new DataSet();
            dbAdap.Fill(ds);
            dt = ds.Tables[0];
           
            return dt;
        }
        public DataTable GetCustermerByUserName(string Username) // used for getting custermer info 
        {
            DataTable dt = new DataTable();

            //create connection 
            OleDbConnection dbConnection = new OleDbConnection();
            dbConnection.ConnectionString = this.connection;

            //build the command 
            OleDbCommand dbCommand = new OleDbCommand();
            dbCommand.CommandText = Sql.GetCustermerByUserName(Username);
            dbCommand.Connection = dbConnection;

            //create adaptor
            OleDbDataAdapter dbAdap = new OleDbDataAdapter();
            dbAdap.SelectCommand = dbCommand;

            //retrieve data and put in database
            DataSet ds = new DataSet();
            dbAdap.Fill(ds);
            dt = ds.Tables[0];

            return dt;
        }
        public DataTable GetCustermerByNRIC(string NRIC) // used for getting custermer info 
        {
            DataTable dt = new DataTable();

            //create connection 
            OleDbConnection dbConnection = new OleDbConnection();
            dbConnection.ConnectionString = this.connection;

            //build the command 
            OleDbCommand dbCommand = new OleDbCommand();
            dbCommand.CommandText = Sql.GetCustermerByNRIC(NRIC);
            dbCommand.Connection = dbConnection;

            //create adaptor
            OleDbDataAdapter dbAdap = new OleDbDataAdapter();
            dbAdap.SelectCommand = dbCommand;

            //retrieve data and put in database
            DataSet ds = new DataSet();
            dbAdap.Fill(ds);
            dt = ds.Tables[0];

            return dt;
        }

        public DataTable GetRoomTypes()
        {
            DataTable dt = new DataTable();

            //create connection 
            OleDbConnection dbConnection = new OleDbConnection();
            dbConnection.ConnectionString = this.connection;

            //build the command 
            OleDbCommand dbCommand = new OleDbCommand();
            dbCommand.CommandText = Sql.GetRoomTypes();
            dbCommand.Connection = dbConnection;

            //create adaptor
            OleDbDataAdapter dbAdap = new OleDbDataAdapter();
            dbAdap.SelectCommand = dbCommand;

            //retrieve data and put in database
            DataSet ds = new DataSet();
            dbAdap.Fill(ds);
            dt = ds.Tables[0];

            return dt; 
        }

        public DataTable GetAvaliableRoom(string RoomType)
        {
            DataTable dt = new DataTable();

            //create connection 
            OleDbConnection dbConnection = new OleDbConnection();
            dbConnection.ConnectionString = this.connection;

            //build the command 
            OleDbCommand dbCommand = new OleDbCommand();
            dbCommand.CommandText = Sql.GetAvaliableRoom(RoomType);
            dbCommand.Connection = dbConnection;

            //create adaptor
            OleDbDataAdapter dbAdap = new OleDbDataAdapter();
            dbAdap.SelectCommand = dbCommand;

            //retrieve data and put in database
            DataSet ds = new DataSet();
            dbAdap.Fill(ds);
            dt = ds.Tables[0];

            return dt;
        }
        public void InsertNewUser(CustomersInfo custermer)
        {
            //create connection 
            OleDbConnection dbConnection = new OleDbConnection();
            dbConnection.ConnectionString = this.connection;

            //build the command 
            OleDbCommand dbCommand = new OleDbCommand();
            dbCommand.CommandText = Sql.InsertNewCustermer();
            dbCommand.Connection = dbConnection;

            // @NRIC,@UserName,@Password,@AccessType,@Name,@Age,@Nationality,@Gender,@NoOfPeople,@RoomNo,@HP,@Address,@CheckInDate,@CheckOutDate
            dbCommand.Parameters.Clear();
            dbCommand.Parameters.AddWithValue("@NRIC",custermer.nric);
            dbCommand.Parameters.AddWithValue("@UserName", custermer.userName);
            dbCommand.Parameters.AddWithValue("@Password", custermer.password);
            dbCommand.Parameters.AddWithValue("@AccessType", custermer.accessType);
            dbCommand.Parameters.AddWithValue("@Name", custermer.name);
            dbCommand.Parameters.AddWithValue("@Age", custermer.age);
            dbCommand.Parameters.AddWithValue("@Nationality", custermer.nationality);
            dbCommand.Parameters.AddWithValue("@Gender", custermer.gender);
            dbCommand.Parameters.AddWithValue("@NoOfPeople", custermer.noOfPeople);
            dbCommand.Parameters.AddWithValue("@RoomNo", custermer.roomNo);
            dbCommand.Parameters.AddWithValue("@HP", custermer.Handphone);
            dbCommand.Parameters.AddWithValue("@Address", custermer.address);
            dbCommand.Parameters.AddWithValue("@CheckInDate", System.DBNull.Value);
            dbCommand.Parameters.AddWithValue("@CheckOutDate", System.DBNull.Value);

            dbConnection.Open();
            dbCommand.ExecuteNonQuery();
            dbConnection.Close();
        }

        public void UploadBookingInfo(RoomBooking Room)
        {
            //create connection 
            OleDbConnection dbConnection = new OleDbConnection();
            dbConnection.ConnectionString = this.connection;

            //build the command 
            OleDbCommand dbCommand = new OleDbCommand();
            dbCommand.CommandText = Sql.UploadBookingInfo();
            dbCommand.Connection = dbConnection;

            // @NoOfPeople,@RoomNo,@CheckInDate,@CheckOutDate,@UserName  
            dbCommand.Parameters.Clear();
            dbCommand.Parameters.AddWithValue("@NoOfPeople",Room.noOfPeople);
            dbCommand.Parameters.AddWithValue("@RoomNo", Room.roomSelection);
            dbCommand.Parameters.AddWithValue("@CheckInDate", Room.checkInDate);
            dbCommand.Parameters.AddWithValue("@CheckOutDate", Room.checkOutDate);
            dbCommand.Parameters.AddWithValue("@UserName", Room.userName);

            dbConnection.Open();
            dbCommand.ExecuteNonQuery();
            dbConnection.Close();
        }
        public void ClearingBookingInfo(RoomBooking Room)
        {
            //create connection 
            OleDbConnection dbConnection = new OleDbConnection();
            dbConnection.ConnectionString = this.connection;

            //build the command 
            OleDbCommand dbCommand = new OleDbCommand();
            dbCommand.CommandText = Sql.UploadBookingInfo();
            dbCommand.Connection = dbConnection;

            // @NoOfPeople,@RoomNo,@CheckInDate,@CheckOutDate,@UserName  
            dbCommand.Parameters.Clear();
            dbCommand.Parameters.AddWithValue("@NoOfPeople", Room.noOfPeople);
            dbCommand.Parameters.AddWithValue("@RoomNo", Room.roomSelection);
            dbCommand.Parameters.AddWithValue("@CheckInDate", System.DBNull.Value);
            dbCommand.Parameters.AddWithValue("@CheckOutDate", System.DBNull.Value);
            dbCommand.Parameters.AddWithValue("@UserName", Room.userName);

            dbConnection.Open();
            dbCommand.ExecuteNonQuery();
            dbConnection.Close();
        }
        public void updateHotelRoom (string SelectedRoom , string OccupantName , int setState)
        {
            //create connection 
            OleDbConnection dbConnection = new OleDbConnection();
            dbConnection.ConnectionString = this.connection;

            //build the command 
            OleDbCommand dbCommand = new OleDbCommand();
            dbCommand.CommandText = Sql.UpdateRoomInfo();
            dbCommand.Connection = dbConnection;

            dbCommand.Parameters.Clear();
            dbCommand.Parameters.AddWithValue("@Occupant", OccupantName);
            dbCommand.Parameters.AddWithValue("@Vaccency", setState);
            dbCommand.Parameters.AddWithValue("@RoomNo", SelectedRoom);

            dbConnection.Open();
            dbCommand.ExecuteNonQuery();
            dbConnection.Close();

        }

        public void UploadFeedback(Feedback feedback)
        {
            //create connection 
            OleDbConnection dbConnection = new OleDbConnection();
            dbConnection.ConnectionString = this.connection;

            //build the command 
            OleDbCommand dbCommand = new OleDbCommand();
            dbCommand.CommandText = Sql.UploadFeedBack();
            dbCommand.Connection = dbConnection;

            dbCommand.Parameters.Clear();

            //insert into quries
            dbCommand.Parameters.AddWithValue("@FeedbackKey", feedback.RanNo);
            dbCommand.Parameters.AddWithValue("@NRIC",feedback.NRIC);
            dbCommand.Parameters.AddWithValue("@Q1",feedback.Q1);
            dbCommand.Parameters.AddWithValue("@Q2",feedback.Q2);
            dbCommand.Parameters.AddWithValue("@Q3",feedback.Q3);
            dbCommand.Parameters.AddWithValue("@BenefitHotel",feedback.BenefitHotel);
            dbCommand.Parameters.AddWithValue("@Comments",feedback.Comments);
            dbCommand.Parameters.AddWithValue("@DoneFeedBack",feedback.Done);
            dbCommand.Parameters.AddWithValue("@Aver",feedback.Ave);

            dbConnection.Open();
            dbCommand.ExecuteNonQuery();
            dbConnection.Close();

        }

        public void DeleteCustomersInfo (string NRIC)
        {
            DataTable dt = new DataTable();

            //create connection 
            OleDbConnection dbConnection = new OleDbConnection();
            dbConnection.ConnectionString = this.connection;

            //build the command 
            OleDbCommand dbCommand = new OleDbCommand();
            dbCommand.CommandText = Sql.DeleteCustomerInfo(NRIC);
            dbCommand.Connection = dbConnection;

            // executing the SQL command
            dbConnection.Open();
            dbCommand.ExecuteNonQuery();
            dbConnection.Close();
        }

    }
}
