﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataManagement
{
    public class CustomersInfo
    {
        private string NRIC;
        private string UserName;
        private string Password;
        private string AccessType = "Custermer";
        private string Name;
        private string Age;
        private string Nationality;
        private string Gender;
        private int NoOfPeople;
        private string RoomNo;
        private string HP;
        private string Address;
        private string CheckInDate;
        private string CheckOutDate;

        public CustomersInfo()
        {
            this.NRIC = "";
            this.Name = "";
            this.Age = "";
            this.Nationality = "";
            this.Gender = "";
            this.NoOfPeople = 0;
            this.RoomNo = "";
            this.HP = "";
            this.Address= "";
            this.CheckInDate = "";
            this.CheckOutDate = "";
        }

        public CustomersInfo(string _NRIC,string _Name,string _Age, string _Nationality, string _Gender, int _NoOfPeople, string _RoomNo, string _HP, string _Address, string _CheckInDate, string _CheckOutDate)
        {
            this.NRIC = _NRIC;
            this.Name = _Name;
            this.Age = _Age;
            this.Nationality = _Nationality;
            this.Gender = _Gender;
            this.NoOfPeople = _NoOfPeople;
            this.RoomNo = _RoomNo;
            this.HP = _HP;
            this.Address = _Address;
            this.CheckInDate = _CheckInDate;
            this.CheckOutDate = _CheckOutDate;
        }

        //string NRIC;
        //string UserName;
        //string Password;
        //string AccessType; // non serializable (set backend)
        //string Name;
        //string Age;
        //string Nationality;
        //string gender;
        //string HP;
        //string Address;
        public CustomersInfo(string _NRIC, string _UserName,string _Password ,string _Name, string _Age, string _Nationality, string _Gender, string _HP, string _Address)
        {
            this.NRIC = _NRIC;
            this.UserName = _UserName;
            this.Password = _Password;

            this.Name = _Name;
            this.Age = _Age;
            this.Nationality = _Nationality;
            this.Gender = _Gender;
            this.NoOfPeople = 0;
            this.RoomNo = "";
            this.HP = _HP;
            this.Address = _Address;
            this.CheckInDate ="";
            this.CheckOutDate ="";
        }

        public string nric
        {
            get { return this.NRIC; }
            set { this.NRIC = value; }
        }
        public string userName
        {
            get { return this.UserName; }
            set { this.UserName = value; }
        }
        public string password 
        {
            get { return this.Password; }
            set { this.Password = value; }
        }

        public string name
        {
            get { return this.Name; }
            set { this.Name = value; }
        }

        public string age
        {
            get { return this.Age; }
            set { this.Age = value; }
        }

        public string nationality
        {
            get { return this.Nationality; }
            set { this.Nationality = value; }
        }

        public string gender
        {
            get { return this.Gender; }
            set { this.Gender = value; }
        }

        public int noOfPeople
        {
            get { return this.NoOfPeople; }
            set { this.NoOfPeople = value; }
        }

        public string roomNo
        {
            get { return this.RoomNo; }
            set { this.RoomNo = value; }
        }

        public string Handphone
        {
            get { return this.HP; }
            set { this.HP= value; }
        }

        public string address
        {
            get { return this.Address; }
            set { this.Address = value; }
        }
        public string checkInDate
        {
            get { return this.CheckInDate; }
            set { this.CheckInDate = value; }
        }
        public string checkOutDate
        {
            get { return this.CheckOutDate; }
            set { this.CheckOutDate = value; }
        }
        public string accessType
        {
            get { return this.AccessType; }
            set { this.AccessType = value; }
        }

    }
}
